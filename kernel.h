#include <opencv2/opencv.hpp>
#include <cuda.h>
#include <cuda_runtime.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
using namespace std;

#include <helper_cuda.h>
#include <helper_timer.h>
#include <helper_image.h>
using namespace cv;


extern "C" void
paint_GPU(const unsigned char *h_src,const unsigned char* h_lastsrc,unsigned char *h_dst,unsigned char *device_dst,int frame_width,int frame_height,int frame_widthStep, int fps, int frameCount);

extern "C" void
GPUnpr(const unsigned char *h_src,unsigned char *h_dst,int frame_width,int frame_height,double *m, char filterMode);