#include "kernel.h"

// Data structure for holding painterly strokes.
class Stroke 
{ 
public:
	// data
	unsigned int radius, x, y;	// Location for the stroke
	unsigned char r, g, b;	// Color
	Stroke *nexts;

	Stroke(void)
	{
		nexts=0;
	}
	Stroke(unsigned int iradius, unsigned int ix, unsigned int iy,	unsigned char ir, unsigned char ig, unsigned char ib)
	{
		radius = iradius;
		x = ix;
		y = iy;
		r = ir;
		g = ig;
		b = ib;
	}

	
};

void cpuBumpFilter(double *m, const unsigned char* host_src, unsigned char* host_dst, int frame_width, int frame_height);

void cpuDespeckleFilter(const unsigned char* host_src, unsigned char* host_dst, int frame_width, int frame_height);

void cpuOilFilter(const unsigned char* host_src, unsigned char* host_dst, int frame_width, int frame_height, int range);

void NPR_Crop_Image(const unsigned char *src_imagedata1,const unsigned char *src_imagedata2,int img_width,int img_height,int img_widthStep,int &xMin,int &xMax,int &yMin,int &yMax);

void NPR_Paint_CPU(const unsigned char *h_src,unsigned char *h_dst,int frame_width,int frame_height,int frame_widthStep);

void Filter_Gaussian_N(unsigned int N ,const unsigned char *src_imagedata,unsigned char *dst_imagedata,int img_width,int img_height,int img_widthStep);

void NPR_Paint_Layer(unsigned char *tCanvas,const unsigned char *tRefImg, int tBrushSize ,int img_width,int img_height,int img_widthStep);

void Paint_Stroke(const Stroke& s,unsigned char *dst_imagedata,int img_width,int img_height,int img_widthStep);