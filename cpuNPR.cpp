﻿#include "cpuNPR.h"

void NPR_Crop_Image(const unsigned char *src_imagedata1,const unsigned char *src_imagedata2,int img_width,int img_height,int img_widthStep,int &xMin,int &xMax,int &yMin,int &yMax)
{
	xMin = img_width-1;
	xMax = 0;
	yMin = img_height-1;
	yMax = 0;
	IplImage *differenceImg = cvCreateImage(cvSize(img_width,img_height),IPL_DEPTH_8U,3);;
	//printf("%d %d %d %d %d %d\n",src_imagedata1[0],src_imagedata2[0],src_imagedata1[1],src_imagedata2[1],src_imagedata1[2],src_imagedata2[2]);
	int tmpC=0;
	for (int y=0;y<img_height;y++)
	{
		for (int x=0;x<img_width;x++)
		{
			int idx = y*img_widthStep+3*x;
			
			differenceImg->imageData[idx] = abs(src_imagedata1[idx]-src_imagedata2[idx]);
			differenceImg->imageData[idx+1] = abs(src_imagedata1[idx+1]-src_imagedata2[idx+1]);
			differenceImg->imageData[idx+2] = abs(src_imagedata1[idx+2]-src_imagedata2[idx+2]);
			//printf("%d-%d-%d",differenceImg->imageData[idx],differenceImg->imageData[idx+1],differenceImg->imageData[idx+2]);
			
			
			int dR,dG,dB;
			dR = abs(src_imagedata1[idx]-src_imagedata2[idx]);
			dG = abs(src_imagedata1[idx+1]-src_imagedata2[idx+1]);
			dB = abs(src_imagedata1[idx+2]-src_imagedata2[idx+2]);
			if(dR+dG+dB<=10)//(!(x>280&&x<335&&y>90&&y<150)&&(dR+dG+dB>10))
			{
				tmpC++;
				//printf("(%d,%d)%d,%d,%d\t",x,y,dR,dG,dB);
			}
			if (dR+dG+dB>10)
			{
				differenceImg->imageData[idx] = 0;
				differenceImg->imageData[idx+1] =0;
				differenceImg->imageData[idx+2] =255;
				//printf("(%d,%d) %d,%d,%d\n",x,y,dR,dG,dB);
				if (xMin > x){xMin = x;}
				if (xMax < x){xMax = x;}
				if (yMin > y){yMin = y;}
				if (yMax < y){yMax = y;}
			}
		}
	}
	printf("tmpC=%d",tmpC);
	cvShowImage("Example2", differenceImg);
	cvWaitKey();
	/*
	if (xMin == img_width-1){xMin = 0;}
	if (xMax == 0){xMax = img_width-1;}
	if (yMin == img_height-1){yMin = 0;}
	if (yMax == 0){yMax = img_height-1;}
	*/
	//if (xMin != 0 || xMax != img_width-1 || yMin != 0 || yMax != img_height-1)
	//{
		printf("%d %d %d %d\n",xMin,xMax,yMin,yMax);
	//}
}

#define levels 256


int cpuClamp(int c)
{
	if (c < 0)
		return 0;
	if (c > 255)
		return 255;
	return c;

}

void cpuBumpFilter(double *m, const unsigned char* host_src, unsigned char* host_dst, int frame_width, int frame_height)
{

	for (int y = 0; y < frame_height; y++)
	{
		for (int x = 0; x < frame_width; x++)
		{
			double r = 0, g = 0, b = 0;

			for (int row = -1; row <= 1; row++)
			{
				int iy = y+row;
				int ioffset;
				if (0 <= iy && iy < frame_height)ioffset = iy*frame_width;
				else ioffset = y*frame_width;

				int moffset = 3*(row+1)+1;
				for (int col = -1; col <= 1; col++)
				{
					double f = m[moffset+col];

					if (f != 0)
					{
						int ix = x+col;
						if (!(0 <= ix && ix < frame_width))
						{
							ix = x;
						}
						//int rgb = inPixels[ioffset+ix];
						//a += f * ((rgb >> 24) & 0xff);
						r += f * host_src[3*(ioffset+ix)+2];
						g += f * host_src[3*(ioffset+ix)+1];
						b += f * host_src[3*(ioffset+ix)];
					}
				}
			}
			//int ia = alpha ? PixelUtils.clamp((int)(a+0.5)) : 0xff;
			int ir = cpuClamp((int)(r+0.5));
			int ig = cpuClamp((int)(g+0.5));
			int ib = cpuClamp((int)(b+0.5));
			//outPixels[index++] = (ia << 24) | (ir << 16) | (ig << 8) | ib;

			host_dst[3*(y*frame_width+x)] = (unsigned char)ib;
			host_dst[3*(y*frame_width+x)+1] = (unsigned char)ig;
			host_dst[3*(y*frame_width+x)+2] = (unsigned char)ir;
		}
	}

}

short cpuPepperAndSalt( short c, short v1, short v2 )
{
	if ( c < v1 )
		c++;
	if ( c < v2 )
		c++;
	if ( c > v1 )
		c--;
	if ( c > v2 )
		c--;
	return c;
}

void cpuDespeckleFilter(const unsigned char* host_src, unsigned char* host_dst, int frame_width, int frame_height)
{

	int index = 0;
	short* r[3];
	short* g[3];
	short* b[3];

	for(int i = 0;i < 3;i++)
	{
		r[i] = new short[frame_width];
		g[i] = new short[frame_width];
		b[i] = new short[frame_width];
	}

	for (int x = 0; x < frame_width; x++)
	{
		r[1][x] = (short)host_src[3*x+2];
		g[1][x] = (short)host_src[3*x+1];
		b[1][x] = (short)host_src[3*x];
	}
	for (int y = 0; y < frame_height; y++)
	{
		bool yIn = y > 0 && y < frame_height-1;
		int nextRowIndex = index+frame_width;
		if ( y < frame_height-1)
		{
			for (int x = 0; x < frame_width; x++)
			{
				r[2][x] = (short)host_src[3*nextRowIndex+2];
				g[2][x] = (short)host_src[3*nextRowIndex+1];
				b[2][x] = (short)host_src[3*nextRowIndex];
				nextRowIndex++;
			}
		}
		for (int x = 0; x < frame_width; x++)
		{
			bool xIn = x > 0 && x < frame_width-1;
			short o_r = r[1][x];
			short o_g = g[1][x];
			short o_b = b[1][x];
			int w = x-1;
			int e = x+1;

			if ( yIn )
			{
				o_r = cpuPepperAndSalt( o_r, r[0][x], r[2][x] );
				o_g = cpuPepperAndSalt( o_g, g[0][x], g[2][x] );
				o_b = cpuPepperAndSalt( o_b, b[0][x], b[2][x] );
			}

			if ( xIn )
			{
				o_r = cpuPepperAndSalt( o_r, r[1][w], r[1][e] );
				o_g = cpuPepperAndSalt( o_g, g[1][w], g[1][e] );
				o_b = cpuPepperAndSalt( o_b, b[1][w], b[1][e] );
			}

			if ( yIn && xIn )
			{
				o_r = cpuPepperAndSalt( o_r, r[0][w], r[2][e] );
				o_g = cpuPepperAndSalt( o_g, g[0][w], g[2][e] );
				o_b = cpuPepperAndSalt( o_b, b[0][w], b[2][e] );

				o_r = cpuPepperAndSalt( o_r, r[2][w], r[0][e] );
				o_g = cpuPepperAndSalt( o_g, g[2][w], g[0][e] );
				o_b = cpuPepperAndSalt( o_b, b[2][w], b[0][e] );
			}

			host_dst[3*index+2] = o_r;
			host_dst[3*index+1] = o_g;
			host_dst[3*index] = o_b;
			index++;
		}
		short *t;
		t = r[0];
		r[0] = r[1];
		r[1] = r[2];
		r[2] = t;
		t = g[0];
		g[0] = g[1];
		g[1] = g[2];
		g[2] = t;
		t = b[0];
		b[0] = b[1];
		b[1] = b[2];
		b[2] = t;
	}

	for(int i = 0;i < 3;i++)
	{
		delete [] r[i];
		delete [] g[i];
		delete [] b[i];
	}
}

void cpuOilFilter(const unsigned char* host_src, unsigned char* host_dst, int frame_width, int frame_height, int range)
{
	int index = 0;
	int rHistogram[levels];
	int gHistogram[levels];
	int bHistogram[levels];
	int rTotal[levels];
	int gTotal[levels];
	int bTotal[levels];

	for (int y = 0; y < frame_height; y++) {
		for (int x = 0; x < frame_width; x++) {
			for (int i = 0; i < levels; i++)
				rHistogram[i] = gHistogram[i] = bHistogram[i] = rTotal[i] = gTotal[i] = bTotal[i] = 0;

			for (int row = -range; row <= range; row++) {
				int iy = y+row;
				int ioffset;
				if (0 <= iy && iy < frame_height) {
					ioffset = iy*frame_width;
					for (int col = -range; col <= range; col++) {
						int ix = x+col;
						if (0 <= ix && ix < frame_width) {
							int r = host_src[3*(ioffset+ix)+2];
							int g = host_src[3*(ioffset+ix)+1];
							int b = host_src[3*(ioffset+ix)];
							int ri = r*levels/256;
							int gi = g*levels/256;
							int bi = b*levels/256;
							rTotal[ri] += r;
							gTotal[gi] += g;
							bTotal[bi] += b;
							rHistogram[ri]++;
							gHistogram[gi]++;
							bHistogram[bi]++;
						}
					}
				}
			}

			int r = 0, g = 0, b = 0;
			for (int i = 1; i < levels; i++) {
				if (rHistogram[i] > rHistogram[r])
					r = i;
				if (gHistogram[i] > gHistogram[g])
					g = i;
				if (bHistogram[i] > bHistogram[b])
					b = i;
			}
			r = rTotal[r] / rHistogram[r];
			g = gTotal[g] / gHistogram[g];
			b = bTotal[b] / bHistogram[b];
			host_dst[3*index+2] = r;
			host_dst[3*index+1] = g;
			host_dst[3*index] = b;
			index++;
		}
	}
}

void NPR_Paint_CPU(const unsigned char *src_imagedata,unsigned char *dst_imagedata,int img_width,int img_height,int img_widthStep)
{
	for (int i=11;i>=3;i=i-2)
	{
		IplImage *tmpImg = cvCreateImage(cvSize(img_width,img_height),IPL_DEPTH_8U,3);
		cvZero(tmpImg);
		Filter_Gaussian_N(i,src_imagedata,(uchar*)tmpImg->imageData,img_width,img_height,img_widthStep);
		NPR_Paint_Layer(dst_imagedata,(uchar*)tmpImg->imageData,i,img_width,img_height,img_widthStep);
		cvReleaseImage(&tmpImg);
	}
	//std::cout << "NPR done." << std::endl;
}

void Filter_Gaussian_N(unsigned int N ,const unsigned char *src_imagedata,unsigned char *dst_imagedata,int img_width,int img_height,int img_widthStep)
{
	int n=N/2;
	if(N<=1||N%2==0)
		return;
	double *t=new double[N*N];
	for(int i=0;i<N;i++)			//²£¥Í¯x°}
		*(t+i)=0;
	*t=1.0;
	*(t+1)=1.0;
	for(int i=0;i<N-2;i++)
		for(int j=i+2;j>0;j--)
			*(t+j)+=*(t+j-1);
	for(int i=1;i<N;i++)
		*(t+i*N)=*(t+i);
	for(int i=1;i<N;i++)
		for(int j=1;j<N;j++)
			*(t+i*N+j)=*(t+i*N)*(*(t+j));
	for(int i=0;i<N;i++)
		for(int j=0;j<N;j++)
			for(int k=0;k<4*n;k++)
				*(t+i*N+j)/=2;				//
	for(int i=0;i<img_height;i++)
	{
		for(int j=0;j<img_width;j++)
		{
			double tr=0.0,tg=0.0,tb=0.0;
			int index = i*img_widthStep+j*3;
			for(int k=-n;k<=n;k++)
			{
				for(int m=-n;m<=n;m++)
				{
					int kt=k,mt=m;//temp
					if(i+k<0)				{kt=-i-k-1;}
					else if(i+k>=img_height){kt=img_height-k-i;}
					if(j+m<0)				{mt=-j-m-1;}
					else if(j+m>=img_width)	{mt=img_width-j-m;}

					int index_shift = kt*img_widthStep+mt*3;

					tr += src_imagedata[index+index_shift + 2]*(*(t+(k+n)*N+m+n));
					tg += src_imagedata[index+index_shift + 1]*(*(t+(k+n)*N+m+n));
					tb += src_imagedata[index+index_shift    ]*(*(t+(k+n)*N+m+n));
				}
			}
			dst_imagedata[index + 2] = tr;
			dst_imagedata[index + 1] = tg;
			dst_imagedata[index    ] = tb;
		}
	}
	delete[] t;
}

void NPR_Paint_Layer(unsigned char *tCanvas,const unsigned char *tRefImg, int tBrushSize ,int img_width,int img_height,int img_widthStep)
{
	Stroke *Shead=new Stroke,*Stail;
	Stail=Shead;
	int ns=0;//number of stroke
	double *d=new double[img_height*img_width];
	for(int i=0;i<img_height;i++)
	{
		for(int j=0;j<img_width;j++)
		{
			int index=i*img_widthStep+j*3;
			double r=(double)tRefImg[index+2]-(double)tCanvas[index+2];
			double g=(double)tRefImg[index+1]-(double)tCanvas[index+1];
			double b=(double)tRefImg[index  ]-(double)tCanvas[index  ];
			d[i*img_width+j]=sqrt(r*r+g*g+b*b);
		}
	}
	for(int i=0;i<img_height;i+=tBrushSize)
	{
		for(int j=0;j<img_width;j+=tBrushSize)
		{
			int index=i*img_width+j;
			double AreaErr=0.0;
			for(int k=-tBrushSize/2;k<=tBrushSize/2;k++)
			{
				for (int m=-tBrushSize/2;m<=tBrushSize/2;m++)
				{
					int kt=k,mt=m;//temp
					if(i+k<0)				{kt=-i-k-1;}
					else if(i+k>=img_height){kt=img_height-k-i;}
					if(j+m<0)				{mt=-j-m-1;}
					else if(j+m>=img_width)	{mt=img_width-j-m;}

					AreaErr+=d[index+kt*img_width+mt];
				}
			}
			AreaErr/=(tBrushSize*tBrushSize);
			if (AreaErr>25.0)
			{
				int x1=j,y1=i;
				for(int k=-tBrushSize/2;k<=tBrushSize/2;k++)
				{
					for (int m=-tBrushSize/2;m<=tBrushSize/2;m++)
					{
						int temp1=index+k*img_width+m,temp2=y1*img_width+x1;
						if(i+k>=0&&i+k<img_height&&j+m>=0&&j+m<img_width)
						{
							if(d[temp1]>d[temp2])
							{
								x1=j+m;
								y1=i+k;
							}
						}
					}
				}
				int tmpIndex = y1*img_widthStep+x1*3;
				Stroke *s=new Stroke;
				s->radius=tBrushSize;
				s->x=j;
				s->y=i;
				s->r=tRefImg[tmpIndex+2];
				s->g=tRefImg[tmpIndex+1];
				s->b=tRefImg[tmpIndex  ];
				Stail->nexts=s;
				Stail=Stail->nexts;
				ns++;
			}
		}
	}
	int nstemp=ns;
	for(int i=0;i<ns;i++)
	{
		int nsp=(rand()%nstemp)+1;//number of stroke to print
		Stroke *stemp=Shead,*tt;//tt -> only for temp
		for(int j=0;j<nsp-1;j++)
			stemp=stemp->nexts;
		Paint_Stroke(*(stemp->nexts),tCanvas,img_width,img_height,img_widthStep);
		tt=stemp->nexts;
		stemp->nexts=tt->nexts;
		delete tt;
		nstemp--;
	}
	delete d;
}

///////////////////////////////////////////////////////////////////////////////
//
//      Helper function for the painterly filter; paint a stroke at
// the given location
//
///////////////////////////////////////////////////////////////////////////////
void Paint_Stroke(const Stroke& s,unsigned char *dst_imagedata,int img_width,int img_height,int img_widthStep) 
{
	int radius_squared = (int)s.radius * (int)s.radius;
	for (int x_off = -((int)s.radius); x_off <= (int)s.radius; x_off++) 
	{
		for (int y_off = -((int)s.radius); y_off <= (int)s.radius; y_off++) 
		{
			int x_loc = (int)s.x + x_off;
			int y_loc = (int)s.y + y_off;

			// are we inside the circle, and inside the image?
			if ((x_loc >= 0 && x_loc < img_width && y_loc >= 0 && y_loc < img_height)) 
			{
				int dist_squared = x_off * x_off + y_off * y_off;
				int offset_rgba = y_loc * img_widthStep + x_loc * 3;

				if (dist_squared <= radius_squared) 
				{
					dst_imagedata[offset_rgba + 2] = s.r;
					dst_imagedata[offset_rgba + 1] = s.g;
					dst_imagedata[offset_rgba    ] = s.b;
				} 
				else if (dist_squared == radius_squared + 1) 
				{
					dst_imagedata[offset_rgba + 2] = (dst_imagedata[offset_rgba + 2] + s.r) / 2;
					dst_imagedata[offset_rgba + 1] = (dst_imagedata[offset_rgba + 1] + s.g) / 2;
					dst_imagedata[offset_rgba    ] = (dst_imagedata[offset_rgba    ] + s.b) / 2;
				}
			}
		}
	}
}
