# This is the README of a CUDA and openCV integrated platform
# In this platform, as long as you have set up the correct path to all the libraries that the nvcc compiler and openCV need, you can easily add CUDA kernel function to perform your image processing effect.

# In the code, we provide three sample filters, bump filter, oil paint filter and master oil paint filter that are implemented both in CPU and GPU version.
# And the source of image frame is from a webcam. However the user can easily switch to a video source or an image source. 

# Last but not least, the platfrom is designed to demonstrate a simple integration of CUDA and openCV for future developers to perform more profound effects. Enjoy and hava fun!

