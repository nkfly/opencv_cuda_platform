#include "kernel.h"
//#include <cuda.h>


#ifndef MAX
#define MAX(a,b) ((a > b) ? a : b)
#endif
#ifndef MIN
#define MIN(a,b) ((a < b) ? a : b)
#endif

#define BLOCK_SIZE 16
#define FILTER_SIZE 9
#define levels 256

__device__ void paintStroke(const unsigned char b, const unsigned char g, const unsigned char r, const int row, const int column, unsigned char* device_dst, const int frame_width, const int frame_height,const int frame_widthStep, const int gaussianN)
{
	for (int qy = MAX(0,(row-gaussianN)); qy < MIN(frame_height,(row+gaussianN+1)); qy++)
	{
		for (int qx = MAX(0,(column-gaussianN)); qx < MIN(frame_width,(column+gaussianN+1)); qx++)
		{
			if((qx-column)*(qx-column) + (qy-row)*(qy-row) <= gaussianN*gaussianN)
			{
				device_dst[qy*frame_widthStep+3*qx] = b;
				device_dst[qy*frame_widthStep+3*qx+1] = g;
				device_dst[qy*frame_widthStep+3*qx+2] = r;
			}
		}
	}
}

__global__ void gaussianFilterKernel(const unsigned char* device_src, unsigned char* device_gdst, const int frame_width, const int frame_height, const int frame_widthStep, const int *GaussianMatrix, const int gaussianN)
{

	const int row = (blockIdx.y * blockDim.y) + threadIdx.y;
    const int column = (blockIdx.x * blockDim.x) + threadIdx.x;
    
	if(row >=  frame_height || column >= frame_width) {return;}

	
	int resultR = 0;
	int resultG = 0;
	int resultB = 0;
	{
		int calNum = 0;
		int countY = MAX(0,-(row-gaussianN));
		for (int qy = MAX(0,(row-gaussianN)); qy < MIN(frame_height,(row+gaussianN+1)); qy++)
		{
			int countX = MAX(0,-(column-gaussianN));
			for (int qx = MAX(0,(column-gaussianN)); qx < MIN(frame_width,(column+gaussianN+1)); qx++)
			{
				int mult = GaussianMatrix[countX]*GaussianMatrix[countY];
				resultR += device_src[qy*frame_widthStep+3*qx]*mult;
				resultG += device_src[qy*frame_widthStep+3*qx+1]*mult;
				resultB += device_src[qy*frame_widthStep+3*qx+2]*mult;
				countX++;
				calNum += mult;
			}
			countY++;
		}
		resultR /= calNum;
		resultG /= calNum;
		resultB /= calNum;
	}
	const int pIdx = row*frame_widthStep+3*column;
	device_gdst[pIdx]   = resultR;
	device_gdst[pIdx+1] = resultG;
	device_gdst[pIdx+2] = resultB;

}
__global__ void calDistanceKernel(const unsigned char* device_gdst, const unsigned char* device_dst,int* device_dmap, const int frame_width, const int frame_height,const int frame_widthStep, const int gaussianN)
{
	const int row = (blockIdx.y * blockDim.y) + threadIdx.y;
    const int column = (blockIdx.x * blockDim.x) + threadIdx.x;
    
	if(row >=  frame_height || column >= frame_width) {return;}

	const int pIdx = row*frame_widthStep+3*column;
	int b = device_gdst[pIdx]-device_dst[pIdx];
	int g = device_gdst[pIdx+1]-device_dst[pIdx+1];
	int r = device_gdst[pIdx+2]-device_dst[pIdx+2];
	int distance = sqrt((double)(b*b+g*g+r*r));
	for (int qy = MAX(0,(row-gaussianN)); qy < MIN(frame_height,(row+gaussianN+1)); qy++)
	{
		for (int qx = MAX(0,(column-gaussianN)); qx < MIN(frame_width,(column+gaussianN+1)); qx++)
		{
			device_dmap[qy*frame_width+qx] += distance;
		}
	}
}
__global__ void calMinusKernel(const unsigned char* device_src, const unsigned char* device_lastsrc, int* device_mmap, const int frame_width, const int frame_height,const int frame_widthStep)
{
	const int row = (blockIdx.y * blockDim.y) + threadIdx.y;
    const int column = (blockIdx.x * blockDim.x) + threadIdx.x;
    
	if(row >=  frame_height || column >= frame_width) {return;}

	const int pIdx = row*frame_widthStep+3*column;
	const int dIdx = row*frame_width+column;
	device_mmap[dIdx] = abs(device_src[pIdx]-device_lastsrc[pIdx]) + abs(device_src[pIdx+1]-device_lastsrc[pIdx+1]) + abs(device_src[pIdx+2]-device_lastsrc[pIdx+2]);
}
__global__ void intensifyKernel(const unsigned char* device_src, unsigned char* device_dst,const int* device_mmap, const int frame_width, const int frame_height, const int frame_widthStep)
{
	const int row = (blockIdx.y * blockDim.y) + threadIdx.y;
    const int column = (blockIdx.x * blockDim.x) + threadIdx.x;
    
	if(row >=  frame_height || column >= frame_width) {return;}

	const int pIdx = row*frame_widthStep+3*column;
	const int idx = row*frame_width+column;

	if(device_mmap[idx] > 50)
	{
		device_dst[pIdx] = device_src[pIdx];
		device_dst[pIdx+1] = device_src[pIdx+1];
		device_dst[pIdx+2] = device_src[pIdx+2];
	}
}
__global__ void paintKernel(const unsigned char* device_src, unsigned char* device_dst,const int* device_dmap,const int* device_mmap, const int frame_width, const int frame_height, const int frame_widthStep, const int gaussianN,const int zeroToPaint=1)
{
	const int row = ((blockIdx.y * blockDim.y) + threadIdx.y)*gaussianN;
    const int column = ((blockIdx.x * blockDim.x) + threadIdx.x)*gaussianN;
    
	if(row >=  frame_height || column >= frame_width) {return;}

	const int pIdx = row*frame_widthStep+3*column;
	const int idx = row*frame_width+column;

	int div = (MIN(frame_height,(row+gaussianN+1))-MAX(0,(row-gaussianN)))*(MIN(frame_width,(column+gaussianN+1))-MAX(0,(column-gaussianN)));
	if(zeroToPaint==0 || (device_mmap[idx] > 20 && device_dmap[idx]/div > 25))
	{
		paintStroke(device_src[pIdx], device_src[pIdx+1], device_src[pIdx+2] ,row ,column, device_dst, frame_width, frame_height, frame_widthStep,gaussianN);
	}
}


extern "C" void
paint_GPU(
             const unsigned char *h_src,
			 const unsigned char* h_lastsrc,
             unsigned char *h_dst,
			 unsigned char *device_dst,
             int frame_width,
             int frame_height,
			 int frame_widthStep,
			 int fps=30,
			 int frameCount=1
             )
{
	int **GaussianMatrix;
	GaussianMatrix = new int*[7];
	for(int i=0;i<7;i++)
	{
		GaussianMatrix[i] = new int[(i+1)*2+1];
		GaussianMatrix[i][0] = 1;
		GaussianMatrix[i][(i+1)*2] = 1;
	}
	GaussianMatrix[0][1] = 2;
	GaussianMatrix[1][1] = 4; GaussianMatrix[1][2] = 6; GaussianMatrix[1][3] = 4;
	GaussianMatrix[2][1] = 6; GaussianMatrix[2][2] = 15; GaussianMatrix[2][3] = 20; GaussianMatrix[2][4] = 15; GaussianMatrix[2][5] = 6;
	GaussianMatrix[3][1] = 8; GaussianMatrix[3][2] = 28; GaussianMatrix[3][3] = 56; GaussianMatrix[3][4] = 70; GaussianMatrix[3][5] = 56; GaussianMatrix[3][6] = 28; GaussianMatrix[3][7] = 8;
	GaussianMatrix[4][1] = 10; GaussianMatrix[4][2] = 45; GaussianMatrix[4][3] = 120; GaussianMatrix[4][4] = 210; GaussianMatrix[4][5] = 252; GaussianMatrix[4][6] = 210; GaussianMatrix[4][7] = 120; GaussianMatrix[4][8] = 45; GaussianMatrix[4][9] = 10;
	GaussianMatrix[5][1] = 12; GaussianMatrix[5][2] = 66; GaussianMatrix[5][3] = 220; GaussianMatrix[5][4] = 495; GaussianMatrix[5][5] = 792; GaussianMatrix[5][6] = 924; GaussianMatrix[5][7] = 792; GaussianMatrix[5][8] = 495; GaussianMatrix[5][9] = 220; GaussianMatrix[5][10] = 66; GaussianMatrix[5][11] = 12;
	GaussianMatrix[6][1] = 14; GaussianMatrix[6][2] = 91; GaussianMatrix[6][3] = 364; GaussianMatrix[6][4] = 1001; GaussianMatrix[6][5] = 2002; GaussianMatrix[6][6] = 3003; GaussianMatrix[6][7] = 3432; GaussianMatrix[6][8] = 3003; GaussianMatrix[6][9] = 2002; GaussianMatrix[6][10] = 1001; GaussianMatrix[6][11] = 364; GaussianMatrix[6][12] = 91; GaussianMatrix[6][13] = 14;
	/*
	GaussianMatrix3	=	{1,2,1};
	GaussianMatrix5	=	{1,4,6,4,1},
	GaussianMatrix7	=	{1,6,15,20,15,6,1},
	GaussianMatrix9	=	{1,8,28,56,70,56,28,8,1},
	GaussianMatrix11=	{1,10,45,120,210,252,210,120,45,10,1};
	*/
	dim3 threads(BLOCK_SIZE, BLOCK_SIZE);
    dim3 grid( (frame_width-1)/threads.x + 1, (frame_height-1)/threads.y + 1);
    
    unsigned char* device_src = NULL;
    cudaMalloc((void**) &device_src, sizeof(unsigned char) * frame_widthStep*frame_height);

	unsigned char* device_lastsrc = NULL;
    cudaMalloc((void**) &device_lastsrc, sizeof(unsigned char) * frame_widthStep*frame_height);
   
	unsigned char* device_gdst = NULL;
    cudaMalloc((void**) &device_gdst, sizeof(unsigned char) * frame_widthStep*frame_height);

	int* device_dmap = NULL;
    cudaMalloc((void**) &device_dmap, sizeof(int) * frame_width*frame_height);

	int* device_mmap = NULL;
    cudaMalloc((void**) &device_mmap, sizeof(int) * frame_width*frame_height);

	cudaMemcpy(device_src, h_src, sizeof(unsigned char)*frame_widthStep*frame_height, cudaMemcpyHostToDevice);
	cudaMemcpy(device_lastsrc, h_lastsrc, sizeof(unsigned char)*frame_widthStep*frame_height, cudaMemcpyHostToDevice);

	int *d_GaussianMatrix;
	cudaMalloc((void**) &d_GaussianMatrix, sizeof(int) * 15);

	calMinusKernel<<<grid, threads >>>(device_src, device_lastsrc, device_mmap, frame_width, frame_height, frame_widthStep);
	
	/*
	int *test = new int[frame_width];
	cudaMemcpy( test, &device_mmap[frame_width*110], sizeof(int)*frame_width, cudaMemcpyDeviceToHost);
	for(int i=0;i<frame_width;i++)
	{
		printf("%d ",test[i]);
	}
	*/
	//cvWaitKey();
	

	for(int r=11;r>=3;r-=2)
	{
		cudaMemset(device_dmap, 0, sizeof(int) * frame_width*frame_height);
		
		if(r==1)
		{
			calMinusKernel<<<grid, threads >>>(device_src, device_dst, device_mmap, frame_width, frame_height, frame_widthStep);
			intensifyKernel<<<grid, threads >>>(device_src,device_dst,device_mmap,frame_width,frame_height,frame_widthStep);
		}
		else
		{
			cudaMemcpy(d_GaussianMatrix,  GaussianMatrix[r/2-1],  sizeof(int)*r,  cudaMemcpyHostToDevice);
			gaussianFilterKernel<<<grid, threads>>>(device_src, device_gdst, frame_width, frame_height, frame_widthStep,d_GaussianMatrix,r/2);
			calDistanceKernel<<<grid, threads >>>(device_gdst, device_dst, device_dmap, frame_width, frame_height, frame_widthStep,r/2);
			paintKernel<<<grid, threads >>>(device_src, device_dst, device_dmap, device_mmap, frame_width, frame_height, frame_widthStep,r/2,frameCount%fps);
		}
		
		/*
		IplImage *testImg = cvCreateImage(cvSize(frame_width,frame_height),IPL_DEPTH_8U,3);
		cudaMemcpy( testImg->imageData, device_dst, sizeof(unsigned char)*frame_widthStep*frame_height, cudaMemcpyDeviceToHost);
		cvShowImage("Show Image",testImg);
		*/
		
		
	}
	cudaMemcpy( h_dst, device_dst, sizeof(unsigned char)*frame_widthStep*frame_height, cudaMemcpyDeviceToHost);
	cudaFree(d_GaussianMatrix);
	cudaFree(device_src);
	cudaFree(device_gdst);
	cudaFree(device_dmap);
	cudaFree(device_mmap);
}

__device__ int clamp(int c){
	if (c < 0)
		return 0;
	if (c > 255)
		return 255;
	return c;

}

__global__ void bumpFilter(double *m, const unsigned char* device_src, unsigned char* device_dst, int frame_width, int frame_height){

	const int pixelRow = (blockIdx.y * blockDim.y) + threadIdx.y;
    const int pixelColumn = (blockIdx.x * blockDim.x) + threadIdx.x;
   
	if(pixelRow >=  frame_height || pixelColumn >= frame_width)return;

	__shared__ double matrix[FILTER_SIZE];

	if(threadIdx.x < FILTER_SIZE)matrix[threadIdx.x] = m[threadIdx.x];

	__syncthreads();

	double r = 0, g = 0, b = 0;

	for (int row = -1; row <= 1; row++)
	{
		int iy = pixelRow+row;
		int ioffset;
		if (0 <= iy && iy < frame_height){ioffset = iy*frame_width;}
		else {ioffset = pixelRow*frame_width;}
		
		int moffset = 3*(row+1)+1;
		for (int col = -1; col <= 1; col++)
		{
			double f = matrix[moffset+col];

			if (f != 0)
			{
				int ix = pixelColumn+col;
				if (!(0 <= ix && ix < frame_width))
				{
						ix = pixelColumn;
				}
				//int rgb = inPixels[ioffset+ix];
				//a += f * ((rgb >> 24) & 0xff);
				r += f * device_src[3*(ioffset+ix)+2];
				g += f * device_src[3*(ioffset+ix)+1];
				b += f * device_src[3*(ioffset+ix)];
			}
		}
	}
	//int ia = alpha ? PixelUtils.clamp((int)(a+0.5)) : 0xff;
	int ir = clamp((int)(r+0.5));
	int ig = clamp((int)(g+0.5));
	int ib = clamp((int)(b+0.5));
	//outPixels[index++] = (ia << 24) | (ir << 16) | (ig << 8) | ib;

	device_dst[3*(pixelRow*frame_width+pixelColumn)] = (unsigned char)ib;
	device_dst[3*(pixelRow*frame_width+pixelColumn)+1] = (unsigned char)ig;
	device_dst[3*(pixelRow*frame_width+pixelColumn)+2] = (unsigned char)ir;

}

__device__ unsigned char pepperAndSalt( unsigned char c, unsigned char v1, unsigned char v2 ) {
		if ( c < v1 )
			c++;
		if ( c < v2 )
			c++;
		if ( c > v1 )
			c--;
		if ( c > v2 )
			c--;
		return c;
	}

__global__ void despeckleFilter(const unsigned char* device_src, unsigned char* device_dst, int frame_width, int frame_height){
	const int pixelRow = (blockIdx.y * blockDim.y) + threadIdx.y;
    const int pixelColumn = (blockIdx.x * blockDim.x) + threadIdx.x;
   
	if(pixelRow >=  frame_height || pixelColumn >= frame_width)return;
	int yIn = pixelRow > 0 && pixelRow < frame_height-1;
	int xIn = pixelColumn > 0 && pixelColumn < frame_width-1;
	unsigned char r = device_src[3*(pixelRow*frame_width+ pixelColumn)+2];
	unsigned char g = device_src[3*(pixelRow*frame_width+ pixelColumn)+1];
	unsigned char b = device_src[3*(pixelRow*frame_width+ pixelColumn)];
	if ( yIn ) {
		r = pepperAndSalt( r, device_src[3*((pixelRow-1)*frame_width+ pixelColumn)+2], device_src[3*((pixelRow+1)*frame_width+ pixelColumn)+2] );
		g = pepperAndSalt( g, device_src[3*((pixelRow-1)*frame_width+ pixelColumn)+1], device_src[3*((pixelRow+1)*frame_width+ pixelColumn)+1] );
		b = pepperAndSalt( b, device_src[3*((pixelRow-1)*frame_width+ pixelColumn)], device_src[3*((pixelRow+1)*frame_width+ pixelColumn)] );
	}

	if ( xIn ) {
		r = pepperAndSalt( r, device_src[3*((pixelRow)*frame_width+ pixelColumn-1)+2], device_src[3*((pixelRow)*frame_width+ pixelColumn+1)+2] );
		g = pepperAndSalt( g, device_src[3*((pixelRow)*frame_width+ pixelColumn-1)+1], device_src[3*((pixelRow)*frame_width+ pixelColumn+1)+1] );
		b = pepperAndSalt( b, device_src[3*((pixelRow)*frame_width+ pixelColumn-1)], device_src[3*((pixelRow)*frame_width+ pixelColumn+1)] );
	}

	if ( yIn && xIn ) {
		r = pepperAndSalt( r, device_src[3*((pixelRow-1)*frame_width+ pixelColumn)+2], device_src[3*((pixelRow+1)*frame_width+ pixelColumn)+2] );
		g = pepperAndSalt( g, device_src[3*((pixelRow-1)*frame_width+ pixelColumn)+1], device_src[3*((pixelRow+1)*frame_width+ pixelColumn)+1] );
		b = pepperAndSalt( b, device_src[3*((pixelRow-1)*frame_width+ pixelColumn)], device_src[3*((pixelRow+1)*frame_width+ pixelColumn)] );

		r = pepperAndSalt( r, device_src[3*((pixelRow)*frame_width+ pixelColumn-1)+2], device_src[3*((pixelRow)*frame_width+ pixelColumn+1)+2] );
		g = pepperAndSalt( g, device_src[3*((pixelRow)*frame_width+ pixelColumn-1)+1], device_src[3*((pixelRow)*frame_width+ pixelColumn+1)+1] );
		b = pepperAndSalt( b, device_src[3*((pixelRow)*frame_width+ pixelColumn-1)], device_src[3*((pixelRow)*frame_width+ pixelColumn+1)] );
	}

	device_dst[3*(pixelRow*frame_width+ pixelColumn)+2] = r;
	device_dst[3*(pixelRow*frame_width+ pixelColumn)+1] = g;
	device_dst[3*(pixelRow*frame_width+ pixelColumn)] = b;


}

__global__ void oilFilter(const unsigned char* device_src, unsigned char* device_dst, int frame_width, int frame_height, int range){
	
    __shared__ unsigned char region[22][3*22];
    __shared__ int sentinelY;
    __shared__ int sentinelX;
    
    int threadId = threadIdx.y*blockDim.x + threadIdx.x;
    const int pixelRow = (blockIdx.y * blockDim.y) + threadIdx.y;
    const int pixelColumn = (blockIdx.x * blockDim.x) + threadIdx.x;
    if(threadId == 0)
	{
    	sentinelY = pixelRow;
    	sentinelX = pixelColumn;    	
    }

    __syncthreads();
    
	if(threadId < 22)
	{
		if(sentinelY - range +threadId >= 0 && sentinelY - range +threadId < frame_height)
		{
			for(int j = 0;j < 22;j++ )
			{
				if(sentinelX - range + j >= 0 && sentinelX - range + j < frame_width)
				{
					region[threadId][3*j] = device_src[3*((sentinelY - range +threadId )*frame_width + sentinelX - range + j)];
					region[threadId][3*j+1] = device_src[3*((sentinelY - range +threadId )*frame_width + sentinelX - range + j)+1];
					region[threadId][3*j+2] = device_src[3*((sentinelY - range +threadId )*frame_width + sentinelX - range + j)+2];
				}	
			}
		}
	}

	__syncthreads();

	
	if(pixelRow >=  frame_height || pixelColumn >= frame_width)return;

	int rHistogram[levels];
	int gHistogram[levels];
	int bHistogram[levels];
	int rTotal[levels];
	int gTotal[levels];
	int bTotal[levels];
	for (int i = 0; i < levels; i++)rHistogram[i] = gHistogram[i] = bHistogram[i] = rTotal[i] = gTotal[i] = bTotal[i] = 0;

	for (int row = -range; row <= range; row++) {
		if (0 <= pixelRow+row && pixelRow+row < frame_height) {
			for (int col = -range; col <= range; col++) {
				if (0 <= pixelColumn+col && pixelColumn+col < frame_width) {
					int r = region[threadIdx.y+row+range][3*(threadIdx.x+col+range)+2];
					int g = region[threadIdx.y+row+range][3*(threadIdx.x+col+range)+1];
					int b = region[threadIdx.y+row+range][3*(threadIdx.x+col+range)];
					int ri = r*levels/256;
					int gi = g*levels/256;
					int bi = b*levels/256;
					rTotal[ri] += r;
					gTotal[gi] += g;
					bTotal[bi] += b;
					rHistogram[ri]++;
					gHistogram[gi]++;
					bHistogram[bi]++;
				}
			}
		}
	}

	int r = 0, g = 0, b = 0;
	for (int i = 1; i < levels; i++) {
		if (rHistogram[i] > rHistogram[r])
			r = i;
		if (gHistogram[i] > gHistogram[g])
			g = i;
		if (bHistogram[i] > bHistogram[b])
			b = i;
	}
	r = rTotal[r] / rHistogram[r];
	g = gTotal[g] / gHistogram[g];
	b = bTotal[b] / bHistogram[b];
	device_dst[3*(pixelRow*frame_width + pixelColumn)+2] = r;
	device_dst[3*(pixelRow*frame_width + pixelColumn)+1] = g;
	device_dst[3*(pixelRow*frame_width + pixelColumn)] = b;

}

extern "C" void
GPUnpr(
             const unsigned char *h_src,
             unsigned char *h_dst,
             int frame_width,
             int frame_height,
			 double *matrix, char filterMode
             )
{
	unsigned char* device_src = NULL;
    cudaMalloc((void**) &device_src, sizeof(unsigned char) * 3 *frame_width*frame_height);
   
    unsigned char* device_dst = NULL;
    cudaMalloc((void**) &device_dst, sizeof(unsigned char) * 3 *frame_width*frame_height);

	dim3 threads(BLOCK_SIZE, BLOCK_SIZE);
    dim3 grid( (frame_width-1)/threads.x + 1, (frame_height-1)/threads.y + 1);
	

	if(filterMode == 'B')
	{// B is pressed, bump filter
		cudaMemcpy(device_src, h_src, sizeof(unsigned char)*3*frame_width*frame_height, cudaMemcpyHostToDevice);
        bumpFilter <<< grid, threads >>>(matrix, device_src, device_dst, frame_width, frame_height);
        despeckleFilter<<< grid, threads >>>(device_dst, device_src, frame_width, frame_height);
        despeckleFilter<<< grid, threads >>>(device_src, device_dst, frame_width, frame_height);
        despeckleFilter<<< grid, threads >>>(device_dst, device_src, frame_width, frame_height);
        despeckleFilter<<< grid, threads >>>(device_src, device_dst, frame_width, frame_height);


        cudaMemcpy( h_dst, device_dst, sizeof(unsigned char)*3*frame_width*frame_height, cudaMemcpyDeviceToHost);

    }
	else if(filterMode == 'O' )
	{// O is pressed, oil filter
    	cudaMemcpy(device_src, h_src, sizeof(unsigned char)*3*frame_width*frame_height, cudaMemcpyHostToDevice);
        oilFilter<<< grid, threads >>>(device_src, device_dst, frame_width, frame_height, 3);

        cudaMemcpy( h_dst, device_dst, sizeof(unsigned char)*3*frame_width*frame_height, cudaMemcpyDeviceToHost);

    }


	
	

}