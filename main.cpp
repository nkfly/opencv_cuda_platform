#include "kernel.h"
#include "cpuNPR.h"

int main(int argc, char** argv) {

    /* Create a window */
    srand(time(NULL));
    cvNamedWindow("NPR", CV_WINDOW_AUTOSIZE);

    /* capture frame from webcam */
    CvCapture* capture = cvCaptureFromCAM( 0 );

    /* Create IplImage to point to each frame and the destination frame */
    IplImage *frame,*processedFrame;
    
    
	/* Initialization of repeatedly-used parameters */
	int frame_width=(int) cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH);   
	int frame_height=(int) cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT);

	processedFrame = cvCreateImage(cvSize(frame_width,frame_height),IPL_DEPTH_8U,3);

	double bumpFilterMatrix[9] =
	{
		-1.0f, -1.0f,  0.0f,
		-1.0f,  1.0f,  1.0f,
		0.0f,  1.0f,  1.0f
	};
	double* GPUbumpMatrix = NULL;
	cudaMalloc((void**) &GPUbumpMatrix, sizeof(double) * 9);
	cudaMemcpy(GPUbumpMatrix, bumpFilterMatrix, sizeof(double)*9, cudaMemcpyHostToDevice);

	char filterMode = 'd';//d is pressed, as the default mode
	char getKey;

	unsigned char *device_dst = NULL;
	IplImage *lastFrame = NULL;


	/* Loop until frame ended or ESC is pressed */
	
    while(1)
	{
        /* grab frame image, and retrieve */
        frame = cvQueryFrame(capture);
        /* exit loop if fram is null / movie end */
        if(!frame) break;


        /* check which filter to apply */
		if( filterMode == 'b')
		{	
			
			cpuBumpFilter(bumpFilterMatrix, (unsigned char*)frame->imageData, (unsigned char*)processedFrame->imageData, frame_width, frame_height);
			cpuDespeckleFilter((unsigned char*)processedFrame->imageData, (unsigned char*)frame->imageData, frame_width, frame_height);
			cpuDespeckleFilter((unsigned char*)frame->imageData, (unsigned char*)processedFrame->imageData, frame_width, frame_height);
			cpuDespeckleFilter((unsigned char*)processedFrame->imageData, (unsigned char*)frame->imageData, frame_width, frame_height);
			cpuDespeckleFilter((unsigned char*)frame->imageData, (unsigned char*)processedFrame->imageData, frame_width, frame_height);
			
		}
		else if(filterMode == 'o')
		{
			cpuOilFilter((unsigned char*)frame->imageData, (unsigned char*)processedFrame->imageData, frame_width, frame_height, 3);
		}
		else if(filterMode == 'p')
		{
			NPR_Paint_CPU((uchar*)frame->imageData, (unsigned char*)processedFrame->imageData, frame_width, frame_height, frame->widthStep);
		}
		else if(filterMode == 'B' || filterMode == 'O')
		{
			GPUnpr((unsigned char*)frame->imageData, (unsigned char*)processedFrame->imageData, frame_width, frame_height, GPUbumpMatrix, filterMode);
		}
		else if(filterMode == 'P')
		{
			if(device_dst == NULL)
			{
				cudaMalloc((void**) &device_dst, sizeof(unsigned char) * frame->widthStep*frame_height);
				cudaMemset(device_dst, 0, sizeof(unsigned char) * frame->widthStep*frame_height);
			}
			cvZero(processedFrame);
			if (lastFrame == NULL)
			{
				lastFrame = cvCreateImage(cvSize(frame_width,frame_height),IPL_DEPTH_8U,3);
				cvZero(lastFrame);
			}
			paint_GPU((uchar*)frame->imageData, (uchar*)lastFrame->imageData, (uchar*)processedFrame->imageData, device_dst, frame_width, frame_height, frame->widthStep, 30, 1);

			cvCopy(frame,lastFrame);
			
		}
		

		if(filterMode == 'b' || filterMode == 'B' || filterMode == 'o' || filterMode == 'O' || filterMode == 'p' || filterMode == 'P')cvShowImage("NPR", processedFrame);
		else cvShowImage("NPR", frame);


		/* if ESC is pressed then exit loop */
		getKey = cvWaitKey(25);// wait 25 miliseconds
		if(getKey == 27)break;
		else if(getKey != -1)filterMode = getKey;
    }

	cudaFree(device_dst);

	
    /* destroy pointer to video */
    cvReleaseCapture(&capture);
	cvReleaseImage(&processedFrame);
    
    /* delete window */
    cvDestroyWindow("NPR");
 
    return EXIT_SUCCESS;
}
